<?php
namespace Plazathemes\Event\Observer\Customer;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Event\Observer;

use Magento\Framework\Event\ObserverInterface;



class Authenticate implements ObserverInterface {

	/** @var \Magento\Framework\Logger\Monolog */
	
    protected $logger;

  public function __construct( \Psr\Log\LoggerInterface $logger ) {


    $this->logger = $logger;
      
    }

   public function execute( Observer $observer)
    { 
      
 
    $customer = $observer->getCustomer();
    $this->logger->debug($customer->getName());

    } 
} 
