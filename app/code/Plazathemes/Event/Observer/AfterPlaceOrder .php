<?php
namespace Plazathemes\Event\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class AfterPlaceOrder implements ObserverInterface
{
    protected $_order;
    public function __construct(
        \Psr\Log\LoggerInterface $order
    ) {
         $this->_order = $order;    
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

          $message= 'PAGE 2 ';
          $this->_order->warn($message);

    
        }
    }

